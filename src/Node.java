public class Node {

    public static Room		initRoom( String slt[], int type )
    {
        Room s = new Room();

        s.id = slt[0];
        s.x = Integer.parseInt(slt[1]);
        s.y = Integer.parseInt(slt[2]);
        s.type = type;
        s.weight = -1;
        s.pop = 0;
        s.links = "";
        s.moveb = 0;
        s.next = null;
        return (s);
    }

    public static void		appenedList(Room s)
    {
        Room	tmp;

        tmp = Main.head;
        if (Main.head == null)
        {
		    Main.head = s;
            return ;
        }
        while (tmp.next != null)
            tmp = tmp.next;
        tmp.next = s;
    }

    public static void		addNode(String info, int type)
    {
        Room    s;
        String  slt[];
        int		n;

        slt = info.split(" ");
        n = slt.length;
        if (n != 3)
            Main.Error("Error reading map!");
        s = Node.initRoom(slt, type);
        Node.appenedList(s);
    }



    public static void		addLink( String[] slt)
    {
        Room	tmp = new Room();
        int		succeded;

        //System.out.println(slt[0] + "-" + slt[1]);
        succeded = 0;
        tmp = Main.head;
        while (tmp != null && succeded != 1)
        {
            if (tmp.id.compareTo(slt[0]) == 0)
            {
                tmp.links = tmp.links + slt[1];
                tmp.links = tmp.links + " ";
                succeded = 1;
            }
            tmp = tmp.next;
        }
        if (succeded == 0)
            Main.Error("File Error!\nA link couldn't be made!\nCheck connections!");
    }
}
