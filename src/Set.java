public class Set {

    public static void SetMapIndexStr(String coord, int value)
    {
        int x;
        int y;

        for (char c : coord.toCharArray())
        {
            if (!Character.isDigit(c))
                return ;
        }
        x = Integer.parseInt(coord) / 1000;
        y = Integer.parseInt(coord) % 1000;
        Main.Map[y][x] = value;
    }

    public static void SetRoomDeatils(Room dst, Room src)
    {
        dst.id = src.id;
        dst.links = src.links;
        dst.type = src.type;
        dst.x = src.x;
        dst.y = src.y;
        dst.weight = src.weight;
        dst.pop = src.pop;
        dst.moveb = src.moveb;
    }

    public static void SwapRoomDeatils(Room a, Room b)
    {
        Room tmp = new Room();

        SetRoomDeatils(tmp, a);
        SetRoomDeatils(a, b);
        SetRoomDeatils(b, tmp);
    }

    public static void SetStartPop (int ants)
    {
        Room tmp;
        Room MainHead;
        Room MainNext;

        tmp = Main.head;
        if (tmp == null)
            return ;
        while (tmp != null)
        {
            if (tmp.type == 1) {
                tmp.pop = ants;

                if (Main.head == tmp)
                    return ;
                SwapRoomDeatils(tmp, Main.head);
            }
            tmp = tmp.next;
        }
    }
}
